package olxbot.scrappers;

public class OlxItem {
    private String link;

    public OlxItem(String link) {
        this.link = link;
    }

    public String getLink() {
        return link;
    }

}
