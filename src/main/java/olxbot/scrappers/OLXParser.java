package olxbot.scrappers;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class OLXParser {

    public List<Document> URLFetcher(String... links) {
        List<Document> linksArr = new ArrayList<>();
        for (String link : links) {
            try {
                linksArr.add(Jsoup.connect(link).get());
            } catch (IOException e) {
                System.err.println(Arrays.toString(e.getStackTrace()));
            }
        }
        return linksArr;
    }

    public List<OlxItem> URLParser(List<Document> documentList) {
        List<OlxItem> linksArr = new ArrayList<>();
        for (Document aDocumentList : documentList) {
            String query = "a.marginright5.link.linkWithHash.detailsLink";

            Elements linksToProduct = aDocumentList.select(query);

            for (Element link : linksToProduct) {
                OlxItem href = new OlxItem(link.attr("href"));
                linksArr.add(href);
            }
        }
        return linksArr;
    }

    public List<String> getLinksFromPropertiesList(List<OlxItem> linksFromSite) {
        List<String> linksFromProperties = new ArrayList<>();

        for (OlxItem olxItem : linksFromSite) {
            linksFromProperties.add(olxItem.getLink());
        }
        return linksFromProperties;
    }
}