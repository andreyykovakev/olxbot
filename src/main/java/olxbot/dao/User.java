package olxbot.dao;

import org.bson.Document;

import java.util.ArrayList;
import java.util.List;

public class User {
    private String name;
    private String surname;
    private long chatId;
    private boolean isReadyForLink;
    private List<String> listLinks;
    private String subscribtionLink;

    public User(long chatId,
                String name,
                String surname,
                List<String> listLinks,
                boolean isReadyForLink,
                String subscribtionLink) {
        this.chatId = chatId;
        this.name = name;
        this.surname = surname;
        this.listLinks = listLinks;
        this.isReadyForLink = isReadyForLink;
        this.subscribtionLink = subscribtionLink;
    }

    public String getSubscribtionLink() {
        return subscribtionLink;
    }

    public boolean isReadyForLink() {
        return isReadyForLink;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public long getId() {
        return chatId;
    }

    public List<String> getListLinks() {
        return listLinks;
    }

    public static User from(Document docUser) {
        Long id = (Long) docUser.getOrDefault(UserDao.ID, 0);
        String name = (String) docUser.getOrDefault(UserDao.NAME, "");
        String surname = (String) docUser.getOrDefault(UserDao.SURNAME, "");
        Boolean isReadyForLink = (Boolean) docUser.getOrDefault(UserDao.READYLINK, false);
        List<String> links = (List<String>) docUser.getOrDefault(UserDao.LINKS, new ArrayList<>());
        String subscribtionLink = docUser.get(UserDao.SUBSCRIBTION_LINK, "");

        return new User(id, name, surname, links, isReadyForLink, subscribtionLink);
    }
}
