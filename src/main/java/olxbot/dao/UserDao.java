package olxbot.dao;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOptions;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.List;

import static com.mongodb.client.model.Updates.*;


public class UserDao {

    public static final String ID = "id";
    public static final String LINKS = "links";
    public static final String READYLINK = "readylink";
    public static final String SUBSCRIBTION_LINK = "subscribtionLink";
    public static final String NAME = "name";
    public static final String SURNAME = "surname";

    private MongoCollection<Document> olxCollection;

    public UserDao(MongoDatabase database) {
        olxCollection = database.getCollection("olx");
    }

    public User updateLinks(User user) {
        Bson filter = Filters.eq(ID, user.getId());

        Bson query = combine(
                pushEach(LINKS, user.getListLinks()),
                set(READYLINK, user.isReadyForLink()),
                set(SUBSCRIBTION_LINK, user.getSubscribtionLink())
        );

        UpdateOptions updateOptions = new UpdateOptions().upsert(true);

        olxCollection.updateOne(filter, query, updateOptions);

        return getUserById(user.getId());
    }

    public User getUserById(long chatId) {
        Bson filter = Filters.eq(ID, chatId);
        Document userDoc = olxCollection.find(filter).first();

        if (userDoc == null) return null;
        return User.from(userDoc);
    }

    public User addUserIfAbsent(long userId, String name, String surname) {
        User dbUser = getUserById(userId);

        if (dbUser == null) {
            Document newUser = new Document()
                    .append(ID, userId)
                    .append(NAME, name)
                    .append(SURNAME, surname);

            insert(newUser);

            return User.from(newUser);
        }

        return dbUser;
    }

    public void insert(Document dbUser) {
        olxCollection.insertOne(dbUser);
    }

    public List<User> getAllUsers() {
        return olxCollection.find()
                .map(User::from)
                .into(new ArrayList<>());
    }
}
