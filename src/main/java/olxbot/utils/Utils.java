package olxbot.utils;

import java.util.ArrayList;
import java.util.List;

public class Utils {

    public static List<String> getUniqueLinks(List<String> linksFromSite, List<String> dBLinks) {
        List<String> newLinks = new ArrayList<>();

        if (dBLinks.isEmpty()) {
            newLinks = linksFromSite;
        } else {
            for (String link : linksFromSite) {
                if (!dBLinks.contains(link)) {
                    newLinks.add(link);
                }
            }
        }
        return newLinks;
    }
}
