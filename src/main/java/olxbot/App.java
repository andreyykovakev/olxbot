package olxbot;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.mongodb.client.MongoDatabase;
import io.dropwizard.Application;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import olxbot.configuration.ApiConfiguration;
import olxbot.configuration.ManagedMongoClient;
import olxbot.controllers.TelegramController;
import olxbot.dao.UserDao;
import olxbot.job.OlxJob;
import olxbot.job.OlxJobRunner;
import olxbot.messenger.*;
import olxbot.scrappers.OLXParser;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;

//ToDo read about codecs in Mongo
public class App extends Application<ApiConfiguration> {
    public static void main(String[] args) throws Exception {
        ApiContextInitializer.init();
        new App().run(args);
    }

    @Override
    public void initialize(Bootstrap<ApiConfiguration> bootstrap) {
        super.initialize(bootstrap);

        bootstrap.setConfigurationSourceProvider(
                new SubstitutingSourceProvider(
                        bootstrap.getConfigurationSourceProvider(),
                        new EnvironmentVariableSubstitutor(false))
        );

        bootstrap.getObjectMapper().setPropertyNamingStrategy(PropertyNamingStrategy.SNAKE_CASE);
    }
//mongodb://olx-kovalev:a44368120842da2388d70d8e4faaf48a@dokku-mongo-olx-kovalev:27017/olx-kovalev
    @Override
    public void run(ApiConfiguration configuration, Environment environment) throws Exception {
        OLXParser olxParser = new OLXParser();

        ManagedMongoClient mongoClient = configuration.getMongo().build();
        environment.lifecycle().manage(mongoClient);
        MongoDatabase olx = mongoClient.getDatabase(configuration.getMongoDB());
        UserDao userDao = new UserDao(olx);

        Bot bot;
        bot = new Bot(configuration.getTlBotConfiguration());


        Messenger messenger = new TelegramMessenger(bot);
        MessageReceiver messageReceiver = new MessageReceiver(messenger, userDao, olxParser);

        if(configuration.isLongPooling()){
            TelegramBotsApi telegramBotsApi = new TelegramBotsApi();
            LongPoolingBot longPoolingBot = new LongPoolingBot(
                    messageReceiver,
                    configuration.getTlBotConfiguration().getBotUsername(),
                    configuration.getTlBotConfiguration().getBotToken());

            telegramBotsApi.registerBot(longPoolingBot);
        }

        OlxJob olxJob = new OlxJob(olxParser, userDao, bot, messenger);

        JobDetail job = JobBuilder.newJob(OlxJobRunner.class)
                .withIdentity("dummyJobName", "group1").build();

        Trigger trigger = TriggerBuilder
                .newTrigger()
                .withIdentity("dummyTriggerName", "group1")
                .withSchedule(
                        SimpleScheduleBuilder
                                .simpleSchedule()
                                .withIntervalInMinutes(5)
                                .repeatForever()
                )
                .build();

        Scheduler scheduler = new StdSchedulerFactory().getScheduler();
        scheduler.getContext().put(OlxJobRunner.NOTIFIER, olxJob);
        scheduler.start();
        scheduler.scheduleJob(job, trigger);

        environment.jersey().register(new TelegramController(messageReceiver));
    }
}
