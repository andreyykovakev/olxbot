package olxbot.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;

public class OlxJobRunner implements Job {
    public static final String NOTIFIER = "notifier";

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        OlxJob olxJob;
        try {
            olxJob = (OlxJob) context.getScheduler().getContext().get(NOTIFIER);
            olxJob.run();
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }
}
