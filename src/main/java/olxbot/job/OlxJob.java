package olxbot.job;

import olxbot.messenger.Messenger;
import olxbot.messenger.entities.TextMessage;
import olxbot.scrappers.OlxItem;
import olxbot.dao.UserDao;
import olxbot.dao.User;
import olxbot.messenger.Bot;
import olxbot.scrappers.OLXParser;
import olxbot.utils.Utils;
import org.jsoup.nodes.Document;

import java.util.List;

public class OlxJob {

    private OLXParser parser;
    private UserDao userDao;
    private Bot bot;
    private Messenger telegramMessenger;

    public OlxJob() {

    }

    public OlxJob(OLXParser parser, UserDao userDao, Bot bot, Messenger telegramMessenger) {
        this.parser = parser;
        this.userDao = userDao;
        this.bot = bot;
        this.telegramMessenger = telegramMessenger;
    }

    public void run() {
        List<User> users = userDao.getAllUsers();
        for (User user : users) {
            if (user.getSubscribtionLink().length() > 2) {
                String subscribtionLink = user.getSubscribtionLink();

                List<Document> documents = parser.URLFetcher(subscribtionLink);
                List<OlxItem> properties = parser.URLParser(documents);
                List<String> siteLinks = parser.getLinksFromPropertiesList(properties);

                if (user.getListLinks().isEmpty()) {
                    userDao.updateLinks(new User(user.getId(), user.getName(), user.getSurname(), siteLinks, user.isReadyForLink(), user.getSubscribtionLink()));
                } else {
                    List<String> uniqueLinks = Utils.getUniqueLinks(siteLinks, user.getListLinks());
                    userDao.updateLinks(new User(user.getId(), user.getName(), user.getSurname(), uniqueLinks, user.isReadyForLink(), user.getSubscribtionLink()));

                    for (String uniqueLink : uniqueLinks) {
                        telegramMessenger.sendMessage(user.getId(), new TextMessage(uniqueLink));
                    }
                }
            }
        }
    }

}
