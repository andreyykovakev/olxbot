package olxbot.controllers;

import olxbot.messenger.MessageReceiver;
import org.telegram.telegrambots.api.objects.Update;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;


@Path("/tl")
public class TelegramController {
    private MessageReceiver messagesReceiver;

    public TelegramController(MessageReceiver messagesReceiver) {
        this.messagesReceiver = messagesReceiver;
    }

    @POST
    @Path("/webhook")
    public Response receive(Update update) {
        messagesReceiver.handle(update);
        return Response.ok("OK").build();
    }
}
