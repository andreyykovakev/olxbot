package olxbot.configuration;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import io.dropwizard.lifecycle.Managed;

public class ManagedMongoClient extends MongoClient implements Managed {

    public ManagedMongoClient(MongoClientURI uri) {
        super(uri);
    }

    @Override
    public void start() {

    }

    @Override
    public void stop() {
        close();
    }
}
