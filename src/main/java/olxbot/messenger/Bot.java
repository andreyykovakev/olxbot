package olxbot.messenger;

import olxbot.configuration.ApiConfiguration;
import org.telegram.telegrambots.ApiContext;
import org.telegram.telegrambots.bots.DefaultAbsSender;
import org.telegram.telegrambots.bots.DefaultBotOptions;

public class Bot extends DefaultAbsSender {

    private ApiConfiguration.TlBotConfiguration configuration;

    public Bot(ApiConfiguration.TlBotConfiguration configuration) {
        super(ApiContext.getInstance(DefaultBotOptions.class));
        this.configuration = configuration;
    }

    public String getBotToken() {
        return configuration.getBotToken();
    }
}