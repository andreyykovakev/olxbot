package olxbot.messenger;

import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;

public class LongPoolingBot extends TelegramLongPollingBot {

    private MessageReceiver messageReceiver;
    private String name;
    private String botToken;

    public LongPoolingBot(MessageReceiver messageReceiver, String name, String botToken) {
        this.messageReceiver = messageReceiver;
        this.name = name;
        this.botToken = botToken;
    }

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            messageReceiver.handle(update);
        }
    }

    @Override
    public String getBotUsername() {
        return this.name;
    }

    @Override
    public String getBotToken() {
        return this.botToken;
    }
}