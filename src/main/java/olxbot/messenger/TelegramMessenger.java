package olxbot.messenger;

import olxbot.messenger.entities.Message;
import olxbot.messenger.entities.TextMessage;
import org.telegram.telegrambots.api.methods.ParseMode;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.exceptions.TelegramApiException;

public class TelegramMessenger implements Messenger {

    private Bot bot;

    public TelegramMessenger(Bot bot) {
        this.bot = bot;
    }

    @Override
    public void sendMessage(long chatId, Message message) {
        if (message instanceof TextMessage) {
            sendTextMessage(chatId, (TextMessage) message);
        }
    }

    private void sendTextMessage(long chatId, TextMessage message) {
        String text = message.getMessage();
        SendMessage sendMessage = new SendMessage().setChatId(chatId).setText(text).setParseMode(ParseMode.MARKDOWN);
        try {
            bot.execute(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }
}
