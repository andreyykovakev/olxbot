package olxbot.messenger.modules;

import olxbot.messenger.Messenger;
import olxbot.dao.User;
import olxbot.dao.UserDao;
import olxbot.messenger.entities.TextMessage;
import olxbot.scrappers.OLXParser;
import olxbot.scrappers.OlxItem;
import org.jsoup.nodes.Document;
import org.telegram.telegrambots.api.objects.Update;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class LinkReceiverModule extends BaseModule {
    private UserDao userDao;
    private OLXParser olxParser;

    public LinkReceiverModule(Messenger messenger, UserDao userDao, OLXParser olxParser) {
        super(messenger);
        this.userDao = userDao;
        this.olxParser = olxParser;

    }

    @Override
    public boolean canHandle(Update update, User user) {
        if (user.isReadyForLink()) {
            return true;
        }
        return update.getMessage().getText().equals("/link");
    }

    @Override
    public void handle(Update update, User user) {
        long chatId = user.getId();
        String name = user.getName();
        String surname = user.getSurname();

        if (user.isReadyForLink()) {
            TextMessage message;
            String link = update.getMessage().getText();
            User user2;

            if(link.trim().startsWith("https://www.olx.ua/")){
                List<Document> documents = olxParser.URLFetcher(link);
                List<OlxItem> properties = olxParser.URLParser(documents);
                List<String> linksFromSite = olxParser.getLinksFromPropertiesList(properties);

                message = new TextMessage("It's all done. I'll let you know if I find anything");
                user2 = new User(chatId, name, surname, linksFromSite, false, link);
                userDao.updateLinks(user2);
            } else {
                message = new TextMessage("You're trying to give me invalid link. Try again");
                user2 = new User(chatId, name, surname, new ArrayList<>(), false, "");
                userDao.updateLinks(user2);
            }

            messenger.sendMessage(user.getId(), message);
            return;
        }

        User user1 = new User(chatId, name, surname, Collections.emptyList(), true, "");
        userDao.updateLinks(user1);

        messenger.sendMessage(user.getId(), new TextMessage("Okay, give me the link"));
    }
}
