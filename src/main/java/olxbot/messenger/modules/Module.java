package olxbot.messenger.modules;

import olxbot.dao.User;
import org.telegram.telegrambots.api.objects.Update;

public interface Module {

    boolean canHandle(Update update, User user);

    void handle(Update update, User user);

}