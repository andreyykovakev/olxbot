package olxbot.messenger.modules;


import olxbot.dao.User;
import olxbot.messenger.Messenger;
import olxbot.messenger.entities.TextMessage;
import org.telegram.telegrambots.api.objects.Update;

public class HelpModule extends BaseModule {

    public HelpModule(Messenger telegramMessenger) {
        super(telegramMessenger);
    }

    @Override
    public boolean canHandle(Update update, User user) {
        return update.getMessage().getText().equals("/help");
    }


    @Override
    public void handle(Update update, User user) {
        long chatId = update.getMessage().getChatId();

        String sb = "/start - first command to start the bot" + "\n" +
                "/link - then insert the link to make bot start looking for new updates" + "\n" +
                "/help - is me. I'm help";

        messenger.sendMessage(chatId, new TextMessage(sb));
    }
}