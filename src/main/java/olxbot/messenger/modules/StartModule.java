package olxbot.messenger.modules;

import olxbot.dao.User;
import olxbot.messenger.Messenger;
import olxbot.messenger.entities.TextMessage;
import org.telegram.telegrambots.api.objects.Update;

public class StartModule extends BaseModule {

    public StartModule(Messenger messenger) {
        super(messenger);
    }

    @Override
    public boolean canHandle(Update update, User user) {
        return update.getMessage().getText().equals("/start");
    }

    @Override
    public void handle(Update update, User user) {
        long chatId = update.getMessage().getChatId();

        String sb = "Hi! I'm a telegram bot. I can notify you whenever olx has new offers according to you request." + "\n" +
                "Hit /help for more info" + "\n";

        messenger.sendMessage(chatId, new TextMessage(sb));

    }
}
