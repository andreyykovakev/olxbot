package olxbot.messenger;

import olxbot.dao.User;
import olxbot.dao.UserDao;
import olxbot.messenger.modules.HelpModule;
import olxbot.messenger.modules.LinkReceiverModule;
import olxbot.messenger.modules.Module;
import olxbot.messenger.modules.StartModule;
import olxbot.scrappers.OLXParser;
import org.telegram.telegrambots.api.objects.Update;

import java.util.Arrays;
import java.util.List;

public class MessageReceiver {
    private List<Module> modules;
    private UserDao userDao;
    private OLXParser olxParser;

    public MessageReceiver(Messenger messenger, UserDao userDao, OLXParser olxParser) {
        modules = Arrays.asList(
                new HelpModule(messenger),
                new StartModule(messenger),
                new LinkReceiverModule(messenger, userDao, olxParser)
        );
        this.userDao = userDao;
        this.olxParser = olxParser;
    }

    public void handle(Update update) {
        long chatId = update.getMessage().getChatId();
        String name = update.getMessage().getChat().getFirstName();
        String surname = update.getMessage().getChat().getLastName();

        User user = userDao.addUserIfAbsent(chatId, name, surname);

        for (Module module : modules) {
            if (module.canHandle(update, user)) {
                module.handle(update, user);
                break;
            }
        }
    }
}
