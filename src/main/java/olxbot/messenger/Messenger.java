package olxbot.messenger;


import olxbot.messenger.entities.Message;

public interface Messenger {

    void sendMessage(long chatId, Message message);

}
